const hexToRgb = (hex) => {
    hex = hex.replace(/^#/, '');
    const colorInt = parseInt(hex, 16);
    const [red, green, blue] = [(colorInt >> 16) & 255, (colorInt >> 8) & 255, colorInt & 255];
    return { red, green, blue };
};

function convertToHex() {
    const rgbInput = document.getElementById('rgbInput').value;
    const rgbValues = rgbInput.split(',').map(value => parseInt(value.trim()));
    const hexColor = rgbValues.reduce((acc, val) => acc + ('0' + val.toString(16)).slice(-2), '#');

    document.getElementById('hexResult').innerText = `Hex Color: ${hexColor}`;
    document.getElementById('hexResult').style.backgroundColor = hexColor;
};

function convertToRgb() {
    const hexInput = document.getElementById('hexInput').value;
    const rgbValues = hexToRgb(hexInput);

    document.getElementById('rgbResult').innerText = `RGB Color: ${rgbValues.red}, ${rgbValues.green}, ${rgbValues.blue}`;
    document.getElementById('rgbResult').style.backgroundColor = `rgb(${rgbValues.red}, ${rgbValues.green}, ${rgbValues.blue})`;
};
